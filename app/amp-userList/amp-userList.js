/**
 * Created by Sajon on 3/25/16.
 */

var app = angular.module("ampTest");

app.directive('ampUserList', ['$filter', function ($filter) {
    return {
        restrict: 'E',
        templateUrl: 'app/amp-userList/amp-userList.html',
        scope:{
            users:'=',
            filter:'=',
        },

        link:function($scope,element,attr){

            // store the original list as usersOrig
            $scope.usersOrig = $scope.users;
            // Apply the filter if it exists
            $scope.users = $filter('filter')($scope.usersOrig, $scope.filter);

            // setup watch to update the list of users with filter
            $scope.$watch('filter', function(val)
            {
                $scope.users = $filter('filter')($scope.usersOrig, val);
            });

            // function to convert .jpg extentions to .png
            $scope.pngImg = function(filename){
                return filename.replace('.jpg','.png');
            }
        }
    };
}]);