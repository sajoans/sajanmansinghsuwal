/**
 * Created by Sajon on 3/25/16.
 */

describe('amp-userList',  function() {
    beforeEach(module('ampTest'));

    var $filter;
    var $compile;
    var $rootScope;
    var $templateCache;

    beforeEach(inject(function(_$compile_, _$rootScope_,_$filter_,_$templateCache_){
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $filter = _$filter_;
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $templateCache = _$templateCache_;


        // Load the template to be tested into $templatecache

        $templateCache.put("app/amp-userList/amp-userList.html", "");

    }));

    describe('amp-user-list', function() {
        it('should filter users correctly', function() {
            // Arrange
            var $scope = $rootScope.$new();
            $scope.users = [
                {
                    'id': '1',
                    'firstName': 'Sean',
                    'lastName': 'Kerr',
                    'picture': 'img/sean.jpg',
                    'Title': 'Senior Developer'
                },
                {
                    'id': '2',
                    'firstName': 'Yaw',
                    'lastName': 'Ly',
                    'picture': 'img/yaw.jpg',
                    'Title': 'AEM Magician'
                }];
            $scope.filter = "";

            var element = $compile('<amp-user-list users="users" filter="filter"></amp-user-list>')($scope);
            $scope.$apply();
            var isolatedScope  =    element.scope();

            // Act
            $scope.filter = "sean";
            isolatedScope.$apply();


            // Assert

            assert.equal(isolatedScope.users.length,1);
            assert.equal(isolatedScope.users[0].id,1);

        });
    });


    describe('amp-user-list', function() {
        it('should return zero users if no match found with filter', function() {
            // Arrange
            var $scope = $rootScope.$new();
            $scope.users = [
                {
                    'id': '1',
                    'firstName': 'Sean',
                    'lastName': 'Kerr',
                    'picture': 'img/sean.jpg',
                    'Title': 'Senior Developer'
                },
                {
                    'id': '2',
                    'firstName': 'Yaw',
                    'lastName': 'Ly',
                    'picture': 'img/yaw.jpg',
                    'Title': 'AEM Magician'
                }];
            $scope.filter = "";

            var element = $compile('<amp-user-list users="users" filter="filter"></amp-user-list>')($scope);
            $scope.$apply();
            var isolatedScope  =    element.scope();

            // Act
            $scope.filter = "seadn";
            isolatedScope.$apply();


            // Assert

            assert.equal(isolatedScope.users.length,0);

        });
    });

});